package rubio.org.app.bcp.beans;

public class CalculoResponse {
	
	private double montoInicial;
	private String monedaOrigen;
    private String monedaDestino;
    private double tipoCambio;
    private double montoFinal;
    
	public double getMontoInicial() {
		return montoInicial;
	}
	public void setMontoInicial(double montoInicial) {
		this.montoInicial = montoInicial;
	}
	public String getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public String getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	public double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public double getMontoFinal() {
		return montoFinal;
	}
	public void setMontoFinal(double montoFinal) {
		this.montoFinal = montoFinal;
	}
    
    

}
