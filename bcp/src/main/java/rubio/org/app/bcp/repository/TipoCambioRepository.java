package rubio.org.app.bcp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rubio.org.app.bcp.model.TipoCambioEntidad;

@Repository
public interface TipoCambioRepository extends JpaRepository<TipoCambioEntidad, Integer> {
	
	TipoCambioEntidad findById(int id);
	
	Optional<TipoCambioEntidad> findByMonedaOrigenAndMonedaDestino(String monedaOrigen, String monedaDestino);
	
}
