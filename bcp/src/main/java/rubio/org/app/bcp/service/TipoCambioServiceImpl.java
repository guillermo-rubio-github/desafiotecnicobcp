package rubio.org.app.bcp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rubio.org.app.bcp.beans.CalculoRequest;
import rubio.org.app.bcp.beans.CalculoResponse;
import rubio.org.app.bcp.beans.TipoCambioRequest;
import rubio.org.app.bcp.beans.TipoCambioResponse;
import rubio.org.app.bcp.model.TipoCambioEntidad;
import rubio.org.app.bcp.repository.TipoCambioRepository;
import rx.Observable;
import rx.Single;

@Service
public class TipoCambioServiceImpl implements ITipoCambioService {

	private List<TipoCambioResponse> listaProvicional;
	
	@Autowired
	private TipoCambioRepository tipoCambioRepository;
	
	public TipoCambioServiceImpl() {
		
		listaProvicional = new ArrayList<TipoCambioResponse>();
		
		TipoCambioResponse data1 = new TipoCambioResponse();
		data1.setId(1);
		data1.setMonedaOrigen("PEN");;
		data1.setMonedaDestino("USD");
		data1.setTipoCambio(0.27);
		listaProvicional.add(data1);
		
		TipoCambioResponse data2 = new TipoCambioResponse();
		data2.setId(2);
		data2.setMonedaOrigen("USD");;
		data2.setMonedaDestino("PEN");
		data2.setTipoCambio(3.75);
		listaProvicional.add(data2);
	}
	
	private TipoCambioResponse convertirEntidadToResponse(TipoCambioEntidad tipoCambioEntidad) {
		TipoCambioResponse tipoCambioResponse = new TipoCambioResponse();
		tipoCambioResponse.setId(tipoCambioEntidad.getId());
		tipoCambioResponse.setMonedaOrigen(tipoCambioEntidad.getMonedaOrigen());
		tipoCambioResponse.setMonedaDestino(tipoCambioEntidad.getMonedaDestino());
		tipoCambioResponse.setTipoCambio(tipoCambioEntidad.getTipoCambio());
		tipoCambioResponse.setUsuario(tipoCambioEntidad.getUsuario());
        return tipoCambioResponse;
    }
	
	private TipoCambioEntidad convertirRequestToEntidad(TipoCambioRequest tipoCambioRequest) {
		TipoCambioEntidad tipoCambioEntidad = new TipoCambioEntidad();
		tipoCambioEntidad.setId(tipoCambioRequest.getId());
		tipoCambioEntidad.setMonedaOrigen(tipoCambioRequest.getMonedaOrigen());
		tipoCambioEntidad.setMonedaDestino(tipoCambioRequest.getMonedaDestino());
		tipoCambioEntidad.setTipoCambio(tipoCambioRequest.getTipoCambio());
		tipoCambioEntidad.setUsuario(tipoCambioRequest.getUsuario());
        return tipoCambioEntidad;
    }
	
	@Override
	public Observable<TipoCambioResponse> listarTodo() {
		return Observable.just(tipoCambioRepository.findAll()).flatMapIterable(x -> x)
				.map(this::convertirEntidadToResponse);
	}
	
	@Override
	public Single<TipoCambioResponse> listarPorId(int id) {
		return Single.just(tipoCambioRepository.findById(id))
				.map(this::convertirEntidadToResponse);
	}
	
	@Override
	public Single<TipoCambioResponse> guardar(TipoCambioRequest tipoCambioRequest) {
		return Single.just(tipoCambioRepository.save(convertirRequestToEntidad(tipoCambioRequest)))
				.map(this::convertirEntidadToResponse);
	}

	@Override
	public Single<TipoCambioResponse> actualizar(TipoCambioRequest tipoCambioRequest) {
		return Single.just(tipoCambioRepository.save(convertirRequestToEntidad(tipoCambioRequest)))
				.map(this::convertirEntidadToResponse);
	}

	@Override
	public Single<CalculoResponse> calcularTipoCambio(CalculoRequest calculoRequest) {
		
		CalculoResponse calculoResponse = null;
		Optional<TipoCambioEntidad> optional = tipoCambioRepository
				.findByMonedaOrigenAndMonedaDestino(calculoRequest.getMonedaOrigen(), calculoRequest.getMonedaDestino());
		
		if (optional.isPresent()) {
			calculoResponse = new CalculoResponse();
			calculoResponse.setMontoInicial(calculoRequest.getMonto());
			calculoResponse.setMonedaOrigen(optional.get().getMonedaOrigen());
			calculoResponse.setMonedaDestino(optional.get().getMonedaDestino());
			calculoResponse.setTipoCambio(optional.get().getTipoCambio());
			calculoResponse.setMontoFinal(optional.get().getTipoCambio() * calculoRequest.getMonto());
		}
		
		return Single.just(calculoResponse);
	}

}
