package rubio.org.app.bcp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rubio.org.app.bcp.beans.AuthResponse;
import rubio.org.app.bcp.utils.JWTUtil;
import rx.Observable;

@RestController
@CrossOrigin
@RequestMapping("/v1/jwt")
public class AuthController {
	
	@Autowired
	private JWTUtil jwtUtil;
	
	@GetMapping()
	public Observable<AuthResponse> sesion() {
		AuthResponse authResponse = new AuthResponse();
		authResponse.setToken(jwtUtil.create("12345", "grubio"));
		return Observable.just(authResponse);
	}


}
