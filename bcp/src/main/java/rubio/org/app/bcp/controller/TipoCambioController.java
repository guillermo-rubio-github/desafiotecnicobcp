package rubio.org.app.bcp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rubio.org.app.bcp.beans.CalculoRequest;
import rubio.org.app.bcp.beans.CalculoResponse;
import rubio.org.app.bcp.beans.TipoCambioRequest;
import rubio.org.app.bcp.beans.TipoCambioResponse;
import rubio.org.app.bcp.service.ITipoCambioService;
import rx.Observable;
import rx.Single;

@RestController
@CrossOrigin
@RequestMapping("/v1/tipoCambio")
public class TipoCambioController {
	
	@Autowired
	private ITipoCambioService iTipoCambioService;
	
	@GetMapping()
	public String prueba() {
		return "Hola mundo";
	}
	
	/**
	@GetMapping(value="/listado")
	public Observable<TipoCambioResponse> listarTodo(@RequestHeader(value = "Authorization") String token){
		Optional<String> idToken = Optional.ofNullable(jwtUtil.getKey(token));
		if (idToken.isPresent()) {
			return iTipoCambioService.listarTodo();
		}
		return Observable.empty();
	}
	**/
	
	@GetMapping(value="/listado")
	public Observable<TipoCambioResponse> listarTodo(){
		return iTipoCambioService.listarTodo();
	}
	
	@GetMapping(value="/listar/{id}")
	public Single<TipoCambioResponse> listarPorId(@PathVariable int id){
			return iTipoCambioService.listarPorId(id);		
	}
	
	@PostMapping(value="/guardar")
	public Single<TipoCambioResponse> guardarTipoCambio(@RequestBody TipoCambioRequest tipoCambioRequest){
			return iTipoCambioService.guardar(tipoCambioRequest);
	}
	
	@PutMapping(value="/actualizar")
	public Single<TipoCambioResponse> actualizarTipoCambio(@RequestBody TipoCambioRequest tipoCambioRequest){
			return iTipoCambioService.actualizar(tipoCambioRequest);
	}
	
	@PostMapping(value="/calcular")
	public Single<CalculoResponse> calcularTipoCambio(@RequestBody CalculoRequest calculoRequest){
			return iTipoCambioService.calcularTipoCambio(calculoRequest);
	}

}
