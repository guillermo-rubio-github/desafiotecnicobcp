package rubio.org.app.bcp.service;

import rubio.org.app.bcp.beans.CalculoRequest;
import rubio.org.app.bcp.beans.CalculoResponse;
import rubio.org.app.bcp.beans.TipoCambioRequest;
import rubio.org.app.bcp.beans.TipoCambioResponse;
import rx.Observable;
import rx.Single;

public interface ITipoCambioService {
	
	Observable<TipoCambioResponse> listarTodo();
	
	Single<TipoCambioResponse> listarPorId(int id);
	
	Single<TipoCambioResponse> guardar(TipoCambioRequest tipoCambioRequest);
	
	Single<TipoCambioResponse> actualizar(TipoCambioRequest tipoCambioRequest);
	
	Single<CalculoResponse> calcularTipoCambio(CalculoRequest calculoRequest);

}
