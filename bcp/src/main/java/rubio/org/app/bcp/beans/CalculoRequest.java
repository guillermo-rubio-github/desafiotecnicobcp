package rubio.org.app.bcp.beans;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class CalculoRequest {
	
	private String monedaOrigen;
    private String monedaDestino;
    private double monto;
    
	public String getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public String getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}

}
