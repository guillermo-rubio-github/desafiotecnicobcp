package rubio.org.app.bcp.beans;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class AuthResponse {
	
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	

}
